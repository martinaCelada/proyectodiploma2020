﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class IdiomaManager
    {
        static List<IIdiomaObserver> observers = new List<IIdiomaObserver>();

        //constructor
        private IdiomaManager()
        {

        }

        private static IdiomaManager idiomaManager;

        //traemos el idioma
        public Entidad.Idioma idioma { get; set; }

        public static IdiomaManager ObtenerInstancia()
        {
            if(idiomaManager == null)
            {
                idiomaManager = new IdiomaManager(); // si no tiene un idioma establecido, se crea uno 
            }

            return idiomaManager;
        }

        //manejo multi-idiomas con patron observer

        public static void Suscribir(IIdiomaObserver o)
        {
            observers.Add(o);
        }
        public static void DesInscribir(IIdiomaObserver o)
        {
            observers.Remove(o);
        }

        private static void Notificar(Entidad.Idioma idioma)
        {
            foreach (var o in observers)
            {
                o.ActualizarIdioma(idioma);
            }
        }

        public static void CambiarIdioma(Entidad.Idioma idioma)
        {
            if (idiomaManager != null)
            {
                idiomaManager.idioma = idioma;
                Notificar(idioma);
            }
        }

        public static void AsignarIdioma(Entidad.Idioma idioma)
        {
            if (idiomaManager != null)
            {
                idiomaManager.idioma = idioma;
            }
        }
    }
}
