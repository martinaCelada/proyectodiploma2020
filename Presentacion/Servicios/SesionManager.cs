﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Servicios
{
    public class SesionManager
    {
        public Entidad.Usuario Usuario { get; set; }
        private static SesionManager _session; //aqui guardamos la unica instancia que va a existir

        //nos devuelve la instancia
        public static SesionManager ObtenerInstancia //controlaremos x este medio que es lo que queremos que suceda
        {
            get
            {
                return _session; //nos devuelve la instancia
            }

        }

        public static void Login(Entidad.Usuario usuario)
        {

            if (_session == null)
            {
                _session = new SesionManager(); //si la sesion es nula, haremos una nueva
                _session.Usuario = usuario;

            }

        }

        public static void Logout()
        {

            if (_session != null)
            {
                _session = null;
            }

        }

        //Creamos el constructor privado
        private SesionManager()
        {

        }
    }
}
