﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class BackupDao
    {
        ConexionSQL conexionSQL = new ConexionSQL();
        public void GenerarBackup(string nameBD, string dd)
        {
            string comando = " USE " + nameBD + ";";
            comando += " BACKUP DATABASE " + nameBD + " TO DISK = 'D:\\database1\\ " + nameBD + "_" + dd + ".Bak' WITH FORMAT, MEDIANAME = 'Z_SQLServerBackups', NAME = 'Full Backup of " + nameBD + "';";
            
            conexionSQL.Ejecutar_Query("ExecuteNonQuery", comando);
        }
    }
}
