﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Datos
{
    public class ConexionSQL
    {
        private SqlCommandBuilder ComBuilder;
        private SqlDataAdapter DataAdapter;
        private DataSet DataSet;
        private static string Conexion = "";
        private SqlConnection SQLCON;
        public bool Armar_Conexion(string servidor, string baseDatos)
        {
            string CON = "Data Source=" + servidor + ";" + "Initial Catalog=" + baseDatos + ";Integrated Security =True";
            SqlConnection SQL = new SqlConnection(CON);
            try
            {
                SQL.Open();
                SQL.Close();
                Conexion = CON;
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool Conectar()
        {
            SQLCON = new SqlConnection(Conexion);
            try
            {
                if (SQLCON.State == ConnectionState.Closed)
                {
                    SQLCON.Open();
                    return true;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private bool Desconectar()
        {
            try
            {
                if (SQLCON.State == ConnectionState.Open)
                {
                    SQLCON.Close();
                    return true;
                }
                else
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public DataSet Consulta_DS(string Consulta, string Tabla)
        {
            try
            {
                Conectar();
                DataSet = new DataSet();
                DataAdapter = new SqlDataAdapter(Consulta, Conexion);
                ComBuilder = new SqlCommandBuilder(DataAdapter);
                DataAdapter.Fill(DataSet, Tabla);
                Desconectar();
                return DataSet;
            }
            catch (Exception ex)
            {
                return null/* TODO Change to default(_) if this is not a reference type */;
            }
        }

        public string Ejecutar_Query(string Tipo, string Consulta)
        {
            try
            {
                SqlCommand CMD = new SqlCommand();
                Conectar();
                CMD.Connection = SQLCON;
                CMD.CommandType = CommandType.Text;
                CMD.CommandText = Consulta;
                string Valor = "";
                if (Tipo == "ExecuteScalar")
                {
                    Valor = CMD.ExecuteScalar().ToString();
                }
                else if (Tipo == "ExecuteNonQuery")
                {
                    Valor = CMD.ExecuteNonQuery().ToString();
                }
                Desconectar();
                return Valor;
            }
            catch (Exception ex)
            {
                Desconectar();
                return null;
            }
        }

        public DataTable ExecuteNonQuery(string sqlString)
        {
            SqlCommand command = new SqlCommand(sqlString);

            try
            {
                DataTable table = new DataTable();

                command.CommandType = System.Data.CommandType.Text;

                if (command.Connection.State == System.Data.ConnectionState.Closed)
                    command.Connection.Open();

                table.Load(command.ExecuteReader());

                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }


}
