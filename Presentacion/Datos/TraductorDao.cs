﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class TraductorDao
    {
        ConexionSQL conexionSQL = new ConexionSQL();
        
        public List<Entidad.Traduccion> ObtenerTraducciones(Entidad.Idioma idioma = null )
        {
            //if(idioma == null)
            //{
            //    idioma = negocio.idiomaBL.obtenerIdiomaPrincipal();
            //}

            List<Entidad.Traduccion> traduccions = new List<Entidad.Traduccion>();

            //DataSet traduccionDS = conexionSQL.Consulta_DS("SELECT t.Traduccion, e.Nombre as Nombre_Etiqueta from Traduccion t INNER JOIN Etiqueta2 e on t.id_etiqueta=e.id WHERE t.id_idioma =" + idioma.Id, "Idioma");

            DataSet traduccionDS = conexionSQL.Consulta_DS("SELECT tablaEtiqueta.id, tablaEtiqueta.Nombre, tablaTraduccion.Traduccion FROM Etiqueta2 as tablaEtiqueta INNER JOIN Traduccion as tablaTraduccion ON tablaEtiqueta.id = tablaTraduccion.id_etiqueta INNER JOIN Idioma as tablaIdioma ON tablaIdioma.Id = tablaTraduccion.id_idioma WHERE tablaIdioma.Nombre = '" + idioma.Nombre +"'" , "Traduccion");


            foreach (DataRow row in traduccionDS.Tables[0].Rows)
            {
                Entidad.Traduccion r = new Entidad.Traduccion
                {
                    Etiqueta = row["Nombre"].ToString(),
                    Descripcion = row["Traduccion"].ToString(),
                };

                traduccions.Add(r);
            }

            return traduccions;
        }
    }
}
