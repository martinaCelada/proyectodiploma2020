﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class RestoreDao
    {
        ConexionSQL conexionSQL = new ConexionSQL();

        public void RealizarRestore(string database, string path)
        {
            string query = "Use master ";
             query +=   "ALTER DATABASE " + database + " SET SINGLE_USER WITH ROLLBACK IMMEDIATE; ";
            query += "RESTORE DATABASE " + database + " FROM DISK = '" + path + "' WITH REPLACE ";
           

            conexionSQL.Ejecutar_Query("ExecuteNonQuery", query);
        }
    }
}
