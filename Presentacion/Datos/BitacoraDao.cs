﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad;

namespace Datos
{
    public class BitacoraDao
    {
        ConexionSQL ConexionSQL = new ConexionSQL();

        public List<Bitacora> listar_bi()
        {
            try
            {
                List<Bitacora> bitacora = new List<Bitacora>();

                DataSet bitacoraDS = ConexionSQL.Consulta_DS("Select ID_bitacora, movimiento, descripcion, ID_criticidad, ID_usuario, fecha_hora, dvh from Biitacora", "Biitacora");
                foreach(DataRow row in bitacoraDS.Tables[0].Rows)
                {
                    Bitacora b = new Bitacora();
                    b.ID_bitacora = Convert.ToInt32(row["ID_bitacora"].ToString());
                    b.movimiento = row["movimiento"].ToString();
                    b.descripcion = row["descripcion"].ToString();
                    b.ID_criticidad = Convert.ToInt32(row["ID_criticidad"].ToString());
                    b.ID_usuario = Convert.ToInt32(row["ID_usuario"].ToString());
                    b.fecha_hora = Convert.ToDateTime(row["fecha_hora"].ToString());

                    bitacora.Add(b);
                }

                return bitacora;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        //public string InsertarBitacora(Bitacora b)
        //{
        //  string respuesta =   ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "INSERT INTO Biitacora VALUES ('" + b.movimiento + "', '"+ b.descripcion +"', '" +b.ID_criticidad + "', '"+ b.ID_usuario +"', '"+ b.fecha_hora +"', '"+ b.dvh +"' ) ");
          
        //  return respuesta;
        //}

        public int Registrar(Entidad.Bitacora b)
        {
            string comando = "INSERT INTO Biitacora (fecha_hora, ID_criticidad, descripcion, ID_usuario, movimiento, DVH) VALUES (GETDATE(), '" + b.ID_criticidad + "', '" + b.descripcion + "', '" + b.ID_usuario + "', '" + b.movimiento + "', '" + b.dvh + "' ) ";

            string respuesta = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", comando);

            return Convert.ToInt32(respuesta);
        }

        public List<Entidad.Bitacora> Consultar(ConsultarBitacoraRequest consulta)
        {
            List<Entidad.Bitacora> registros = new List<Bitacora>();
            string comando = "SELECT b.Id_bitacora, b.fecha_hora, u.nombre as Users, c.descripcion as Criticidad, b.descripcion, b.ID_usuario, b.DVH FROM Biitacora b INNER JOIN Criticidad c on c.ID_criticidada = b.ID_criticidad LEFT JOIN Users u on u.ID_usuario = b.ID_usuario";

            string sqlwhere = " WHERE b.Fecha BETWEEN " + "'" + Convert.ToDateTime(consulta.FechaDesde).ToShortDateString() + "'" + " AND " + "'" + Convert.ToDateTime(consulta.FechaHasta).ToShortDateString() + " 23:59:59.999 " + "'";

            if(consulta.IdCriticidad != null)
            {
                sqlwhere += " AND c.id =" + consulta.IdCriticidad;

            }else if(consulta.IdUsuario != 0)
            {
                sqlwhere += " AND b.Id_Usuario =" + consulta.IdUsuario;
            }

            comando += sqlwhere;

            DataSet bitacoraDS = ConexionSQL.Consulta_DS(comando, "Biitacora");
            foreach(DataRow row in bitacoraDS.Tables[0].Rows)
            {
                Entidad.Bitacora r = new Bitacora
                {
                    ID_bitacora = Convert.ToInt32(row["ID_bitacora"].ToString()),
                    ID_usuario = Convert.ToInt32(row["ID_usuario"].ToString()),
                    ID_criticidad = Convert.ToInt32(row["ID_criticidad"].ToString()),
                    movimiento = row["movimiento"].ToString(),
                    descripcion = row["descripcion"].ToString(),
                    fecha_hora = Convert.ToDateTime(row["fecha_hora"].ToString()),
                    dvh = row["DVH"].ToString()

                };

                registros.Add(r);
            }

            return registros;
        }
    }
}
