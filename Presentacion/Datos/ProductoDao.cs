﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad;
using System.Data;

namespace Datos
{
    public class ProductoDao
    {
        ConexionSQL ConexionSQL = new ConexionSQL();
        
        public List<Producto> listar_prod()
        {
            try
            {
                List<Producto> productos = new List<Producto>();

                DataSet productosDS = ConexionSQL.Consulta_DS("Select ID_Producto, nombre_producto, descripcion, stock, precio_compra, precio_venta from Producto", "Producto");
                foreach (DataRow row in productosDS.Tables[0].Rows)
                {
                    Producto p = new Producto(); //entidad
                    p.ID_producto = Convert.ToInt32(row["ID_Producto"].ToString());
                    p.nombre_producto = row["nombre_producto"].ToString();
                    p.stock = Convert.ToInt32(row["stock"].ToString());
                    p.precio_compra = Convert.ToInt32(row["precio_compra"].ToString());
                    p.precio_venta = Convert.ToInt32(row["precio_venta"].ToString());
                    p.descripcion = row["descripcion"].ToString();

                    productos.Add(p);
                }

                return productos;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertarProducto(Producto p)
        {
            string value = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "INSERT INTO Producto VALUES ('"  + p.nombre_producto + "', '" + p.descripcion + "', " + p.stock + ", " + p.precio_compra + ", " + p.precio_venta + ")");

            if(value != "")
            {
                return true;
            }
            else
            {
                return false;
            }

            
        }

        public bool EditarProducto(Producto p)
        {
            string value = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "UPDATE Producto set nombre_producto='" + p.nombre_producto +"', descripcion= '"+ p.descripcion +"', stock= '"+ p.stock +"', precio_compra='"+ p.precio_compra +"', precio_venta='"+ p.precio_venta +"' FROM Producto where ID_producto= '"+ p.ID_producto +"' ");

            if (value != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EliminarProducto(Producto p)
        {
            string value = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "DELETE FROM Producto where ID_producto= '"+ p.ID_producto +"'");

            if (value != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
