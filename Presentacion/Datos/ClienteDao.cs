﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad;

namespace Datos
{
    public class ClienteDao
    {
        ConexionSQL ConexionSQL = new ConexionSQL();

        public List<Cliente> listar_cli()
        {
            try
            {
                List<Cliente> clientes = new List<Cliente>();

                DataSet clientesDS = ConexionSQL.Consulta_DS("Select ID_cliente, nombre, apellido, DNI, email from Cliente", "Cliente");
                foreach(DataRow row in clientesDS.Tables[0].Rows)
                {
                    Cliente c = new Cliente();
                    c.ID_cliente = Convert.ToInt32(row["ID_cliente"].ToString());
                    c.nombre = row["nombre"].ToString();
                    c.apellido = row["apellido"].ToString();
                    c.DNI = Convert.ToInt32(row["DNI"].ToString());
                    c.email = row["email"].ToString();

                    clientes.Add(c);
                }

                return clientes;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertarCliente(Cliente c)
        {
            string value = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "INSERT INTO Cliente VALUES ('" + c.nombre + "', '" + c.apellido + "', " + c.DNI + ", '" + c.email + "')");

            if (value != "")
            {
                return true;
            }
            else
            {
                return false;
            }


        }

        public bool EditarCliente(Cliente c)
        {
            string value = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "UPDATE Cliente set nombre='" + c.nombre + "', apellido= '" + c.apellido + "', DNI= '" + c.DNI + "', email='" + c.email + "' FROM Cliente where ID_cliente= '" + c.ID_cliente + "' ");

            if (value != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool EliminarCliente(Cliente c)
        {
            string value = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "DELETE FROM Cliente where ID_cliente= '" + c.ID_cliente + "'");

            if (value != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
 