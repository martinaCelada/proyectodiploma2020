﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad;
using Servicios;

namespace Datos
{
    public class LoginDao
    {
        ConexionSQL conexionSQL = new ConexionSQL();
        
        public Entidad.Usuario ConsultarPorNombre(string user)
        {
            Entidad.Usuario usuario = new Usuario();
            try
            {
                DataSet usuarioDS = conexionSQL.Consulta_DS("Select * from Users where userName = '" + user + "'", "Users");

                usuario.ID_usuario = Convert.ToInt32(usuarioDS.Tables[0].Rows[0]["ID_usuario"].ToString());
                usuario.loginName = usuarioDS.Tables[0].Rows[0]["userName"].ToString();
                usuario.password1 = usuarioDS.Tables[0].Rows[0]["passUser"].ToString();
                usuario.nombre = usuarioDS.Tables[0].Rows[0]["nombre"].ToString();
                usuario.apellido = usuarioDS.Tables[0].Rows[0]["apellido"].ToString();
                usuario.dvh = usuarioDS.Tables[0].Rows[0]["DVH"].ToString();
                //usuario.reintentos = Convert.ToInt32(usuarioDS.Tables[0].Rows[0]["reintentos"].ToString());
                //usuario.eliminado = Convert.ToBoolean(usuarioDS.Tables[0].Rows[0]["eliminado"].ToString());


            }
            catch (Exception)
            {

                throw;
            }
            
            return usuario;

        }

        public string ConsultarPorPass(Entidad.Usuario usuario)
        {

            try
            {
                 conexionSQL.Ejecutar_Query("ExecuteScalar", "Select passUser from Users where userName = '" + usuario.password1 + "'");               
               
            }
            catch(Exception )
            {
                throw;
            }

            return usuario.password1;
        }

        public bool login(string user, string pass)
        {
            //valida que existe el usuario
            string Usu = conexionSQL.Ejecutar_Query("ExecuteScalar", "Select ID_usuario from Users where userName = '" + user + "' ");

            if (Usu != "")
            {
                string Contra = conexionSQL.Ejecutar_Query("ExecuteScalar", "Select passUser from Users where userName = '" + user + "'");
                //valida que la contraseña es la correcta segun el usuario
                if (pass == Contra)
                {
                    List<Usuario> usuario = new List<Usuario>();

                    DataSet usuarioDS = conexionSQL.Consulta_DS("Select ID_usuario, userName, passUser, position, nombre, apellido, locked from Users", "Users");
                    //foreach(DataRow row in usuarioDS.Tables[0].Rows)
                    //{
                    //    Usuario u = new Usuario();
                    //    u.ID_usuario = Convert.ToInt32(row["ID_usuario"].ToString());
                    //    u.loginName = row["userName"].ToString();
                    //    u.password1 = row["passUser"].ToString();
                    //    u.position = row["position"].ToString();
                    //    u.nombre = row["nombre"].ToString();
                    //    u.apellido = row["apellido"].ToString();                        

                    //    usuario.Add(u);
                    //    //Usamos el patron singleton para el login
                    //    SesionManager.Login(u);
                    //}

                    return true;
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        //se fija los usuarios blqueados
        public bool Locked(string user)
        {
            string userLock = conexionSQL.Ejecutar_Query("ExecuteScalar", "Select locked from Users where userName = '"+ user +"' ");

            if(userLock == "False")
            {
                return false; //usuario no bloqueado
            }
            else
            {                
                return true; //usuario bloqueado
            }
        }

        //bloquea la cuenta
        public void LockAccount(string user)
        {
            conexionSQL.Ejecutar_Query("ExecuteScalar", "UPDATE Users set locked = 1 where userName = '" + user + "' ");
        }

        public Entidad.Usuario TraerUsuarioPorNombre(Entidad.Usuario usuario)
        {
            try
            {
                Entidad.Usuario user = new Usuario();
                DataSet usuarioDS = conexionSQL.Consulta_DS("Select ID_usuario, userName, passUser, position, nombre, apellido, locked from Users where userName = '" + usuario.loginName + "'" , "Users");

                user.ID_usuario = Convert.ToInt32(usuarioDS.Tables[0].Rows[0][" ID_usuario"].ToString());

                return user;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int Registrar(Entidad.Usuario usuario)
        {
            string comando = "UPDATE Users set DVH = ('" + usuario.dvh + "') From Users where ID_usuario = '" + usuario.ID_usuario + "'";

            string respuesta = conexionSQL.Ejecutar_Query("ExecuteNonQuery", comando);

            return Convert.ToInt32(respuesta);
        }
    }
}
