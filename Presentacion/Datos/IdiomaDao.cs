﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class IdiomaDao
    {
        ConexionSQL conexionSQL = new ConexionSQL();

        public List<Entidad.Idioma> ListarIdiomas()
        {
            List<Entidad.Idioma> idiomas = new List<Entidad.Idioma>();

            DataSet idiomaDS = conexionSQL.Consulta_DS("SELECT * from Idioma", "Idioma");

            foreach (DataRow row in idiomaDS.Tables[0].Rows)
            {
                Entidad.Idioma r = new Entidad.Idioma
                {
                    Id = Convert.ToInt32(row["Id"].ToString()),
                    Nombre = row["Nombre"].ToString(),
                    Principal = (bool)row["Principal"],
                };

                idiomas.Add(r);
            }

            return idiomas;

        }
    }
}
