﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Datos
{
    public class DigitoVerificadorDao
    {
        ConexionSQL ConexionSQL = new ConexionSQL();

        public string ConsultarDVV(string tabla)
        {
          return  ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "SELECT DVV FROM DVV WHERE Tabla = '" + tabla + "'");
        }

        public int ActualizarDVV(string cadena, string tabla)
        {
            string value = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "UPDATE DVV SET DVV = '" + cadena + "' WHERE Tabla = '" + tabla + "'");
            
            //se devuelve la cadena en formma de int
            return Convert.ToInt32(value);
        }

        public int ActualizarDVH(string cadena, string tabla, string id)
        {
            string value = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "UPDATE " + tabla + " SET DVH = '" + cadena + "' WHERE Id = '" + id + "'");
           
            //se devuelve la cadena en formma de int
            return Convert.ToInt32(value);
        }

        public DataTable ConsultarDVH(string tabla)
        {
           return ConexionSQL.ExecuteNonQuery("SELECT DVH FROM '" + tabla);       
        }        

        public DataTable ListarDVV() 
        {
            return ConexionSQL.ExecuteNonQuery("SELECT * FROM DVV");
        }

        public DataTable ConsultarTabla(string tabla) 
        {
            return ConexionSQL.ExecuteNonQuery("SELECT * FROM '" + tabla);
        }

        public int ActualizarDVH(Entidad.Usuario usuario)
        {
            string value = ConexionSQL.Ejecutar_Query("ExecuteNonQuery", "UPDATE Users SET DVH = '" + usuario.dvh + "' from Users WHERE ID_usuario = '" + usuario.ID_usuario + "'");

            //se devuelve la cadena en formma de int
            return Convert.ToInt32(value);
        }

    }
}
