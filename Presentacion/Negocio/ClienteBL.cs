﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidad;

namespace Negocio
{
    public class ClienteBL
    {
        ClienteDao clienteDao = new ClienteDao();

        public List<Cliente> Listar_clientes()
        {
            try
            {
                List<Cliente> clientes = clienteDao.listar_cli();

                return clientes;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertarC(Cliente c)
        {
            return clienteDao.InsertarCliente(c);
        }

        public bool EditarC(Cliente c)
        {
            return clienteDao.EditarCliente(c);
        }

        public bool EliminarC(Cliente c)
        {
            return clienteDao.EliminarCliente(c);
        }
    }
}
