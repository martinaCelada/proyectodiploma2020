﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Datos;
using Entidad;

namespace Negocio
{
    public class BitacoraBL
    {
        BitacoraDao bitacoraDao = new BitacoraDao();
        DigitoVerificadorBL DigitoVerificadorBL = new DigitoVerificadorBL();

        //public void AltaBitacora(Bitacora b)
        //{
        //    bitacoraDao.InsertarBitacora(b);
        //}

        public List<Bitacora> listar()
        {
            try
            {
                List<Bitacora> bitacora = bitacoraDao.listar_bi();
                return bitacora;

            }catch(Exception ex)
            {
                return null;
            }
        }

        public int Registrar(Entidad.Bitacora bitacora)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted })) //no se que hace esto
            {
                //calculamos los dvh y se lo asignamos a la propiedad 
                //bitacora.dvh = DigitoVerificadorBL.CalcularDVH(this.ConcatenarRegistro(bitacora));

                //continuamos con el registro
               int result = bitacoraDao.Registrar(bitacora);

                //verifica los digitos
                //DigitoVerificadorBL.ActualizarDVV("DVV");

                scope.Complete();

                return result;
            }
        }

        public List<Entidad.Bitacora> Consultar(ConsultarBitacoraRequest requerimiento)
        {
            return bitacoraDao.Consultar(requerimiento);
        }

        private string ConcatenarRegistro(Entidad.Bitacora bitacora)
        {
            return bitacora.fecha_hora.ToString() + bitacora.ID_criticidad + bitacora.descripcion + bitacora.ID_usuario;
        }

    }
}
