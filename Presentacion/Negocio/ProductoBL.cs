﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;
using Entidad;

namespace Negocio
{
    public class ProductoBL
    {
        ProductoDao ProductoDao = new ProductoDao();
        
        public List<Producto> Listar_productos()
        {
            try
            {
                List<Producto> productos = ProductoDao.listar_prod();

                return productos;

            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public bool InsertarP(Producto p)
        {
            return ProductoDao.InsertarProducto(p);
        }

        public bool EditarP(Producto p)
        {
            return ProductoDao.EditarProducto(p);        
        }

        public bool EliminarP(Producto p)
        {
            return ProductoDao.EliminarProducto(p);
        }
    }
}
