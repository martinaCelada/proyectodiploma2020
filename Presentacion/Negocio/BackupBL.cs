﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocio
{
    public class BackupBL
    {
        BackupDao backupDao = new BackupDao();

        public void NuevoBackup(string bdname, string dd)
        {
            backupDao.GenerarBackup(bdname, dd);
        }
    }
}
