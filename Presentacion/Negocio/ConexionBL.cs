﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocio
{
    public class ConexionBL
    {
        ConexionSQL conexion = new ConexionSQL();

        public void ConnectionBD(string server, string bd)
        {
            conexion.Armar_Conexion(server, bd);
        }
    }
}
