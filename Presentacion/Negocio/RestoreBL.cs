﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocio
{
    public class RestoreBL
    {
        RestoreDao restoreDao = new RestoreDao();

        public void RealizarRestore(string database, string path)
        {
            restoreDao.RealizarRestore(database, path);
        }
    }
}
