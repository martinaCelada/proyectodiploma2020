﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio
{
   public  class IdiomaBL
    {

        private Datos.IdiomaDao IdiomaDao;

        public IdiomaBL()
        {
            IdiomaDao = new Datos.IdiomaDao();
        }

        public List<Entidad.Idioma> listarIdioma()
        {
            return IdiomaDao.ListarIdiomas();
        }

        public Entidad.Idioma ObtenerIdiomaPrincipal()
        {
            return IdiomaDao.ListarIdiomas().FirstOrDefault(i => i.Principal);
        }
    }
}
