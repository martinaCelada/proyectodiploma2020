﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Datos;

namespace Negocio
{
    public class TraductorBL
    {
        private Datos.TraductorDao TraductorDao;

        public TraductorBL()
        {
            TraductorDao = new TraductorDao();
        }

        public List<Entidad.Traduccion> ObtenerTraducciones(Entidad.Idioma idioma)
        {
            return TraductorDao.ObtenerTraducciones(idioma);
        }
    }
}
