﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Datos;
using Entidad;
using Servicios;

namespace Negocio
{
    public class LoginBL
    {
        LoginDao loginDao = new LoginDao();
        Entidad.Usuario usuario = new Entidad.Usuario();
        DigitoVerificadorBL DigitoVerificadorBL = new DigitoVerificadorBL();

        public Entidad.Usuario LogIn(string user, string pass)
        {

           Entidad.Usuario usuario = loginDao.ConsultarPorNombre(user);

            //encriptacion la contraseña
            string PassEncrip = Encriptacion.GetHA256(pass);

            if(usuario.password1 == PassEncrip)
            {
                //si ambas contraseñas son correctas, hace el login
                //Uso del patron singleton para el inicio de sesión
                VerificarDatos(usuario);
                SesionManager.Login(usuario);
            }          

            return usuario;
        }

        //verifica si el socio esta bloqueado o no 
        public bool IsLocked(string user)
        {
            return loginDao.Locked(user);
        }

        //bloqueo cuenta
        public void LockAccount(string user)
        {
            loginDao.LockAccount(user);
        }

        //Verificar los datos
        public int VerificarDatos(Entidad.Usuario usuario)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted })) //no se que hace esto
            {
                //calculamos los dvh y se lo asignamos a la propiedad 
                usuario.dvh = this.ConcatenarRegistro(usuario);

                int result = DigitoVerificadorBL.CalcularDVH(usuario);

                //inserto el dvh 
                //int result = loginDao.Registrar(usuario);
                scope.Complete();
                
                return result;
            }
        }

        //Con este metodo, al traer los datos del usuario, los concateno y me devuelve un nro.
        private string ConcatenarRegistro(Entidad.Usuario usuario)
        {            
            string cadena = usuario.ID_usuario + usuario.nombre + usuario.apellido + usuario.loginName + usuario.password1;
            double resultado = 0;
            foreach(char c in cadena)
            {
                resultado += char.GetNumericValue(c);
            }

            return resultado.ToString();
        }
    }
}
