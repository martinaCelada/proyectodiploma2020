﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Datos;

namespace Negocio
{
    public class DigitoVerificadorBL
    {
        private DigitoVerificadorDao DigitoVerificadorDao;

        public DigitoVerificadorBL()
        {
            DigitoVerificadorDao = new DigitoVerificadorDao();
        }

        //CONSULTAR
        public string ConsultarDVV(string tabla)
        {
            return DigitoVerificadorDao.ConsultarDVV(tabla);
        }

        //ACTUALIZAR
        public int ActualizarDVV(string tabla)
        {
            try
            {
                //se trae los dvh de la tabla
                DataTable table = DigitoVerificadorDao.ConsultarDVH(tabla);

                //se trae los dvv de la  tabla
                string dvv = this.ConsultarDVV(tabla);

                //actualiza los datos de la tabla consultada 
                return DigitoVerificadorDao.ActualizarDVV(dvv, tabla);

            }catch(Exception ex)
            {
                throw ex;
            }
        }

        // CALCULAR 
        public int CalcularDVH(Entidad.Usuario usuario)
        {
            // return Servicios.Encriptacion.GenerarHash(registro);

           return  DigitoVerificadorDao.ActualizarDVH(usuario);
        }

        public string CalcularDVV(DataTable tabla)
        {
            StringBuilder text = new StringBuilder();

            foreach (DataRow row in tabla.Rows)
            {
                text.Append(row["DVH"].ToString());
            }

            return Servicios.Encriptacion.GenerarHash(text.ToString());
        }
        
        //COMPARAR
        public bool CompararDVV(string tabla, string dvv)
        {
            DataTable table = DigitoVerificadorDao.ConsultarDVH(tabla);

            return this.CalcularDVV(table) == dvv;
        }
        public bool CompararDVH(string registro, string dvh)
        {
            return Servicios.Encriptacion.GenerarHash(registro) == dvh;
        }

        //-------------------------------------------------------
        public bool RecalcularDVV()
        {
            try
            {
                using(var scope = new TransactionScope()) //no entiendo para que se usa esto
                {
                    DataTable dvvs = DigitoVerificadorDao.ListarDVV();

                    foreach(DataRow row in dvvs.Rows)
                    {
                        DataTable tabla = DigitoVerificadorDao.ConsultarTabla(row["Nombre_Tabla"].ToString());

                        //Saco el solo lectura y permito null (PK)
                        if (tabla.Columns.Contains("Id"))
                        {
                            tabla.Columns["Id"].ReadOnly = false;
                            tabla.Columns["Id"].AllowDBNull = true;
                        }

                        if (tabla.Columns.Contains("DVH")) tabla.Columns.Remove("DVH");

                        foreach(DataRow r in tabla.Rows)
                        {
                            string id = r["Id"].ToString();
                            r["Id"] = DBNull.Value;

                           // DigitoVerificadorDao.ActualizarDVH(this.CalcularDVH(Servicios.Encriptacion.ConcatenarRegistro(r)),row["Nombre_Tabla"].ToString(), id);
                        }

                        this.ActualizarDVV(row["Nombre_Tabla"].ToString());
                    }

                    scope.Complete();
                }

                return true;

            }catch(Exception ex)
            {
                return false;
            }
        }

        public bool VerificarIntegridad()
        {
            try
            {
                bool resultado = true;
                DataTable dvvs = DigitoVerificadorDao.ListarDVV();

                foreach(DataRow row in dvvs.Rows)
                {
                    DataTable tabla = DigitoVerificadorDao.ConsultarTabla(row["Nombre_Tabla"].ToString());

                    if(!this.CompararDVV(row["Nombre_Tabla"].ToString(), (row["DVV"].ToString()))){

                        //registrar en bitacora

                        resultado = false;
                    }                   

                    //Saco el solo lectura y permito null (PK)
                    if (tabla.Columns.Contains("Id"))
                    {
                        tabla.Columns["Id"].ReadOnly = false;
                        tabla.Columns["Id"].AllowDBNull = true;
                    }

                    foreach(DataRow r in tabla.Rows)
                    {
                        string dvh = r["DVH"].ToString();
                        string id = r["Id"].ToString();

                        // Dejo vacio el dvh y el id para no tenerlo en cuenta en la generacion y comparacion.
                        r["DVH"] = string.Empty;
                        r["Id"] = DBNull.Value;

                        if(!this.CompararDVH(Servicios.Encriptacion.ConcatenarRegistro(r), dvh))
                        {
                            //registrar en bitacora

                            resultado = true;
                        }
                    }
                }

                return resultado;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}
