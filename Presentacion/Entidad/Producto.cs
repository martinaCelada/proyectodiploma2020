﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    public class Producto
    {
        public int ID_producto { get; set; }

        public string nombre_producto { get; set; }

        public string descripcion { get; set; }

        public int stock { get; set; }

        public decimal precio_compra { get; set; }

        public decimal precio_venta { get; set; }

        //public List<Producto> productos { get; set; }
    }
}
