﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    public class Cliente
    {
        public int ID_cliente { get; set; }

        public string nombre { get; set; }

        public string apellido { get; set; }

        public int DNI { get; set; }

        public string email { get; set; }

    }
}
