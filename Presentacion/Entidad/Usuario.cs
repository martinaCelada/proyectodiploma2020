﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    public class Usuario
    {
        public Usuario()
        {
            permisos = new List<Permiso>();
        }
        public int ID_usuario { get; set; }

        public string loginName { get; set; }

        public string password1 { get; set; }

        //public string position { get; set; }

        public string nombre { get; set; }

        public string apellido { get; set; }
       
        public int reintentos { get; set; }

        public bool eliminado { get; set; }

        public bool locked { get; set; }

        public string dvh { get; set; }

        public List<Permiso> permisos { get; set; }
    }
}
