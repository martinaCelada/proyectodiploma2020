﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    public class Bitacora
    {
        public int ID_bitacora { get; set; }
        public string movimiento { get; set; }

        public string descripcion { get; set; }

        public int ID_criticidad { get; set; }

        public int ID_usuario { get; set; }

        public DateTime  fecha_hora { get; set; }

        public string dvh { get; set; }
    }
}
