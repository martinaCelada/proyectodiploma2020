﻿namespace Presentación
{
    partial class frmCambiarIdioma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblCambioIdioma = new System.Windows.Forms.Label();
            this.rdbEspañol = new System.Windows.Forms.RadioButton();
            this.rdbIngles = new System.Windows.Forms.RadioButton();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblCambioIdioma
            // 
            this.lblCambioIdioma.AutoSize = true;
            this.lblCambioIdioma.Location = new System.Drawing.Point(28, 35);
            this.lblCambioIdioma.Name = "lblCambioIdioma";
            this.lblCambioIdioma.Size = new System.Drawing.Size(200, 13);
            this.lblCambioIdioma.TabIndex = 0;
            this.lblCambioIdioma.Text = "Seleccione el idioma que desea cambiar:";
            // 
            // rdbEspañol
            // 
            this.rdbEspañol.AutoSize = true;
            this.rdbEspañol.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rdbEspañol.Location = new System.Drawing.Point(165, 85);
            this.rdbEspañol.Name = "rdbEspañol";
            this.rdbEspañol.Size = new System.Drawing.Size(63, 17);
            this.rdbEspañol.TabIndex = 17;
            this.rdbEspañol.TabStop = true;
            this.rdbEspañol.Text = "Español";
            this.rdbEspañol.UseVisualStyleBackColor = true;
            this.rdbEspañol.CheckedChanged += new System.EventHandler(this.rdbEspañol_CheckedChanged);
            // 
            // rdbIngles
            // 
            this.rdbIngles.AutoSize = true;
            this.rdbIngles.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rdbIngles.Location = new System.Drawing.Point(31, 85);
            this.rdbIngles.Name = "rdbIngles";
            this.rdbIngles.Size = new System.Drawing.Size(53, 17);
            this.rdbIngles.TabIndex = 16;
            this.rdbIngles.TabStop = true;
            this.rdbIngles.Text = "Ingles";
            this.rdbIngles.UseVisualStyleBackColor = true;
            this.rdbIngles.CheckedChanged += new System.EventHandler(this.rdbIngles_CheckedChanged);
            // 
            // btnAceptar
            // 
            this.btnAceptar.Location = new System.Drawing.Point(31, 145);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(75, 23);
            this.btnAceptar.TabIndex = 18;
            this.btnAceptar.Text = "Aceptar";
            this.btnAceptar.UseVisualStyleBackColor = true;
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(165, 145);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 19;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // frmCambiarIdioma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(327, 199);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.rdbEspañol);
            this.Controls.Add(this.rdbIngles);
            this.Controls.Add(this.lblCambioIdioma);
            this.Name = "frmCambiarIdioma";
            this.Text = "frmCambiarIdioma";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmCambiarIdioma_FormClosing);
            this.Load += new System.EventHandler(this.frmCambiarIdioma_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblCambioIdioma;
        private System.Windows.Forms.RadioButton rdbEspañol;
        private System.Windows.Forms.RadioButton rdbIngles;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnCancelar;
    }
}