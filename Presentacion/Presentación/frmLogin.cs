﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;
using Entidad;
using Servicios;
using static Entidad.Enums;

namespace Presentación
{
    public partial class frmLogin : Form, IIdiomaObserver
    {
        int contador = 0;
        LoginBL loginBL = new LoginBL();
        private IdiomaManager IdiomaManager;
        private Entidad.Idioma idioma;
        DigitoVerificadorBL DigitoVerificadorBL = new DigitoVerificadorBL();
        BitacoraBL BitacoraBL = new BitacoraBL();


        public frmLogin()
        {
            InitializeComponent();
            txtContraseña.PasswordChar = '*';   //hace que la contraseña no sea visible

        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            this.IdiomaManager = Servicios.IdiomaManager.ObtenerInstancia();
            Servicios.IdiomaManager.Suscribir(this);

        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {

            if (!string.IsNullOrEmpty(txtUsuario.Text) && !string.IsNullOrEmpty(txtContraseña.Text))
            {
                try
                {
                    //valida que el login es correcto
                    loginBL.LogIn(txtUsuario.Text, txtContraseña.Text);

                    //obtiene los datos de la sesion;
                    Servicios.SesionManager sesion = SesionManager.ObtenerInstancia;

                    if (!loginBL.IsLocked(txtUsuario.Text))     //si el usuario no esta bloqueado, entra
                    {
                        contador = 0;
                        //bool verificarIntegridad = DigitoVerificadorBL.VerificarIntegridad();

                        //se regristra el usuario logueado en la bitacora
                        BitacoraBL.Registrar(new Entidad.Bitacora
                        {
                            ID_criticidad = (int)Criticidad.Baja,
                            movimiento = "Inicio Sesión",
                            descripcion = $"Usuario logueado con exito {sesion.Usuario.nombre} ",
                            ID_usuario = sesion.Usuario.ID_usuario,
                            fecha_hora = DateTime.Now
                        });

                        MenuPrincipal mp = new MenuPrincipal();
                        mp.Show();
                        this.Close();
                    }
                    else
                    {

                        // si el usuario pone bien sus datos, pero esta bloqueado en la  bd.
                        BitacoraBL.Registrar(new Entidad.Bitacora
                        {
                            ID_criticidad = (int)Criticidad.Baja,
                            descripcion = "Usuario bloqueado " + sesion.Usuario.nombre,
                            ID_usuario = sesion.Usuario.ID_usuario,
                            fecha_hora = DateTime.Now
                        });
                        MessageBox.Show("Su cuenta se encuentra bloqueada, contacte con un Administrador");
                    }

                }
                catch
                {
                    //cuenta los errores
                    contador++;
                    MessageBox.Show("Login error!");
                }

                if (contador == 3) //si son 3 errores, bloqueo el usuario
                {
                    loginBL.LockAccount(txtUsuario.Text);

                    // si el usuario pone bien sus datos, pero esta bloqueado en la  bd.
                    //BitacoraBL.Registrar(new Entidad.Bitacora
                    //{
                    //    ID_criticidad = (int)Criticidad.Baja,
                    //    descripcion = "Usuario bloqueado " + sesion.Usuario.nombre,
                    //    ID_usuario = sesion.Usuario.ID_usuario,
                    //    fecha_hora = DateTime.Now.Date
                    //});
                    MessageBox.Show("Su cuenta se encuentra bloqueada, contacte con un Administrador");
                }
            }

            else
            {
                if (rdbIngles.Checked == true)
                {
                    MessageBox.Show("You must complete username and password to continue");
                }
                else if (rdbEspañol.Checked == true)
                {
                    MessageBox.Show("Complete su usuario y contraseña para poder continuar");
                }
            }
        }


        private void frmLogin_FormClosing(object sender, FormClosingEventArgs e)
        {
            Servicios.IdiomaManager.DesInscribir(this);
        }

        public void ActualizarIdioma(Entidad.Idioma idioma)
        {
            this.Traducir(idioma);
        }

        private void Traducir(Entidad.Idioma idioma = null)
        {
            Negocio.TraductorBL traductorBL = new TraductorBL();

            var traducciones = traductorBL.ObtenerTraducciones(idioma);

            foreach (Control control in this.Controls)
            {
                if (traducciones.Any(t => t.Etiqueta == control.Name))
                {
                    control.Text = traducciones.FirstOrDefault(t => t.Etiqueta == control.Name).Descripcion;
                }
            }
        }

        private void rdbIngles_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbIngles.Checked == true)
            {
                idioma = new Idioma
                {
                    Id = 2,
                    Nombre = "Ingles",
                    Principal = false

                };

                IdiomaManager.CambiarIdioma(idioma);
            }
        }

        private void rdbEspañol_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbEspañol.Checked == true)
            {
                idioma = new Idioma
                {
                    Id = 1,
                    Nombre = "Español",
                    Principal = true
                };

                IdiomaManager.CambiarIdioma(idioma);
            }
        }
    }
}





