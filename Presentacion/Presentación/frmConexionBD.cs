﻿using Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación
{
    public partial class frmConexionBD : Form
    {
        ConexionBL ConexionBL = new ConexionBL();
        public frmConexionBD()
        {
            InitializeComponent();
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {
            string server = txtServer.Text;
            string bd = txtBD.Text;

            if (!string.IsNullOrEmpty(txtServer.Text) && !string.IsNullOrEmpty(txtBD.Text))
            {
                ConexionBL.ConnectionBD(server, bd);
                MessageBox.Show("Successful connection. / Conexion exitosa.");                
                frmLogin frmLog = new frmLogin();
                frmLog.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("You must complete the data to continue / Debe completar los datos para poder continuar");
            }
        }
    }
}
