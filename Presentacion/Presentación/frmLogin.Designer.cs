﻿namespace Presentación
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnIngresar = new System.Windows.Forms.Button();
            this.rdbEspañol = new System.Windows.Forms.RadioButton();
            this.rdbIngles = new System.Windows.Forms.RadioButton();
            this.selectLanguage = new System.Windows.Forms.Label();
            this.txtContraseña = new System.Windows.Forms.TextBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.lblContraseña = new System.Windows.Forms.Label();
            this.lblNombreUsuario = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCancelar
            // 
            this.btnCancelar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnCancelar.Location = new System.Drawing.Point(201, 238);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 17;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnIngresar
            // 
            this.btnIngresar.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnIngresar.Location = new System.Drawing.Point(32, 238);
            this.btnIngresar.Name = "btnIngresar";
            this.btnIngresar.Size = new System.Drawing.Size(75, 23);
            this.btnIngresar.TabIndex = 16;
            this.btnIngresar.Text = "Ingresar";
            this.btnIngresar.UseVisualStyleBackColor = true;
            this.btnIngresar.Click += new System.EventHandler(this.btnIngresar_Click);
            // 
            // rdbEspañol
            // 
            this.rdbEspañol.AutoSize = true;
            this.rdbEspañol.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rdbEspañol.Location = new System.Drawing.Point(168, 198);
            this.rdbEspañol.Name = "rdbEspañol";
            this.rdbEspañol.Size = new System.Drawing.Size(63, 17);
            this.rdbEspañol.TabIndex = 15;
            this.rdbEspañol.TabStop = true;
            this.rdbEspañol.Text = "Español";
            this.rdbEspañol.UseVisualStyleBackColor = true;
            this.rdbEspañol.CheckedChanged += new System.EventHandler(this.rdbEspañol_CheckedChanged);
            // 
            // rdbIngles
            // 
            this.rdbIngles.AutoSize = true;
            this.rdbIngles.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.rdbIngles.Location = new System.Drawing.Point(168, 149);
            this.rdbIngles.Name = "rdbIngles";
            this.rdbIngles.Size = new System.Drawing.Size(53, 17);
            this.rdbIngles.TabIndex = 14;
            this.rdbIngles.TabStop = true;
            this.rdbIngles.Text = "Ingles";
            this.rdbIngles.UseVisualStyleBackColor = true;
            this.rdbIngles.CheckedChanged += new System.EventHandler(this.rdbIngles_CheckedChanged);
            // 
            // selectLanguage
            // 
            this.selectLanguage.AutoSize = true;
            this.selectLanguage.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.selectLanguage.Location = new System.Drawing.Point(29, 153);
            this.selectLanguage.Name = "selectLanguage";
            this.selectLanguage.Size = new System.Drawing.Size(111, 13);
            this.selectLanguage.TabIndex = 13;
            this.selectLanguage.Text = "Seleccione un idioma:";
            // 
            // txtContraseña
            // 
            this.txtContraseña.Location = new System.Drawing.Point(134, 77);
            this.txtContraseña.Name = "txtContraseña";
            this.txtContraseña.Size = new System.Drawing.Size(142, 20);
            this.txtContraseña.TabIndex = 12;
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(134, 23);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(142, 20);
            this.txtUsuario.TabIndex = 11;
            // 
            // lblContraseña
            // 
            this.lblContraseña.AutoSize = true;
            this.lblContraseña.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblContraseña.Location = new System.Drawing.Point(29, 80);
            this.lblContraseña.Name = "lblContraseña";
            this.lblContraseña.Size = new System.Drawing.Size(64, 13);
            this.lblContraseña.TabIndex = 10;
            this.lblContraseña.Text = "Contraseña:";
            // 
            // lblNombreUsuario
            // 
            this.lblNombreUsuario.AutoSize = true;
            this.lblNombreUsuario.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblNombreUsuario.Location = new System.Drawing.Point(29, 26);
            this.lblNombreUsuario.Name = "lblNombreUsuario";
            this.lblNombreUsuario.Size = new System.Drawing.Size(99, 13);
            this.lblNombreUsuario.TabIndex = 9;
            this.lblNombreUsuario.Text = "Nombre de usuario:";
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(357, 303);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnIngresar);
            this.Controls.Add(this.rdbEspañol);
            this.Controls.Add(this.rdbIngles);
            this.Controls.Add(this.selectLanguage);
            this.Controls.Add(this.txtContraseña);
            this.Controls.Add(this.txtUsuario);
            this.Controls.Add(this.lblContraseña);
            this.Controls.Add(this.lblNombreUsuario);
            this.Name = "frmLogin";
            this.Text = "frmLogin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmLogin_FormClosing);
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnIngresar;
        private System.Windows.Forms.RadioButton rdbEspañol;
        private System.Windows.Forms.RadioButton rdbIngles;
        private System.Windows.Forms.Label selectLanguage;
        private System.Windows.Forms.TextBox txtContraseña;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Label lblContraseña;
        private System.Windows.Forms.Label lblNombreUsuario;
    }
}