﻿using Entidad;
using Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;

namespace Presentación
{
    public partial class frmBackup : Form, IIdiomaObserver
    {
        private Servicios.SesionManager session;
        BackupBL backup = new BackupBL();

        public frmBackup()
        {
            InitializeComponent();
        }

        private void frmBackup_Load(object sender, EventArgs e)
        {
            try
            {
                session = Servicios.SesionManager.ObtenerInstancia;
                this.Traducir();
                IdiomaManager.Suscribir(this);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.BeginInvoke(new MethodInvoker(this.Close));
            }
        }        

        private void btnBackup_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime d = DateTime.Now;
                string dd = d.Day + "-" + d.Month;
               
                backup.NuevoBackup(txtbd.Text, dd);

                //registrar en la bitacora

                MessageBox.Show("Se ha guardo el backup de la base de datos de forma correcta", "OK", MessageBoxButtons.OK, MessageBoxIcon.Information);

                txtbd.Clear();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void Traducir()
        {
            TraductorBL traductor = new TraductorBL();
            var idiomaManager = IdiomaManager.ObtenerInstancia();

            var traducciones = traductor.ObtenerTraducciones(idiomaManager.idioma);

            foreach (Control item in this.Controls)
            {
                if (traducciones.Any(t => t.Etiqueta == item.Name))
                {
                    item.Text = traducciones.FirstOrDefault(t => t.Etiqueta == item.Name).Descripcion;
                }

                TraducirControlesInternos(item, traducciones);
            }
        }

        private void TraducirControlesInternos(Control item, List<Traduccion> traducciones)
        {
            if (item is GroupBox)
            {
                foreach (Control subItem in item.Controls)
                {
                    if (traducciones.Any(t => t.Etiqueta == subItem.Name))
                    {
                        subItem.Text = traducciones.FirstOrDefault(t => t.Etiqueta == subItem.Name).Descripcion;
                    }

                    TraducirControlesInternos(subItem, traducciones);
                }
            }
        }

        public void ActualizarIdioma(Idioma idioma)
        {
            this.Traducir();
        }

        private void frmBackup_FormClosing(object sender, FormClosingEventArgs e)
        {
            IdiomaManager.DesInscribir(this);
        }
    }
}
