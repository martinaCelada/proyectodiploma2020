﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad;
using Servicios;

namespace Presentación
{
    public partial class MenuPrincipal : Form, IIdiomaObserver
    {
        public MenuPrincipal()
        {
            InitializeComponent();
        }

        private void productosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmProductos frmProductos = new frmProductos();
            frmProductos.Show();
        }

        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCliente frmCliente = new frmCliente();
            frmCliente.Show();
        }

        private void bitacoraToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitacora frmBitacora = new Bitacora();
            frmBitacora.Show();
        }

        private void MenuPrincipal_Load(object sender, EventArgs e)
        {
            this.Traducir();
            Servicios.IdiomaManager.Suscribir(this);
        }

        public void ActualizarIdioma(Idioma idioma)
        {
            this.Traducir();
        }

        private void Traducir()
        {
            Negocio.TraductorBL traductorBL = new Negocio.TraductorBL();
            var idiomaManager = Servicios.IdiomaManager.ObtenerInstancia();

            var traducciones = traductorBL.ObtenerTraducciones(idiomaManager.idioma);

            //menu
            this.menuInicio.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.menuInicio.Name).Descripcion;
            this.menuIngresos.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.menuIngresos.Name).Descripcion;
            this.menuVentas.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.menuVentas.Name).Descripcion;
            this.menuConsultas.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.menuConsultas.Name).Descripcion;
            this.menuReporte.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.menuReporte.Name).Descripcion;
            this.menuConfiguracion.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.menuConfiguracion.Name).Descripcion;
            this.menuHerramientas.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.menuHerramientas.Name).Descripcion;

            //sub-menus
            this.subMenuSalir.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.subMenuSalir.Name).Descripcion;
            this.subMenuProductos.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.subMenuProductos.Name).Descripcion;
            this.subMenuRegistroVentas.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.subMenuRegistroVentas.Name).Descripcion;
            this.subMenuClientes.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.subMenuClientes.Name).Descripcion;
            this.subMenuDetalleVenta.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.subMenuDetalleVenta.Name).Descripcion;
            this.subMenuReporteProducto.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.subMenuReporteProducto.Name).Descripcion;
            this.SubMenuAdministrarFamilia.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.SubMenuAdministrarFamilia.Name).Descripcion;
            this.SubMenuAdministrarUsuario.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.SubMenuAdministrarUsuario.Name).Descripcion;
            this.SubMenuAdministrarPatente.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.SubMenuAdministrarPatente.Name).Descripcion;
            this.SubMenuIdioma.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.SubMenuIdioma.Name).Descripcion;
            this.SubMenuBackup.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.SubMenuBackup.Name).Descripcion;
            this.SubMenuRestore.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.SubMenuRestore.Name).Descripcion;
            this.SubMenuCambioContraseña.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.SubMenuCambioContraseña.Name).Descripcion;
            this.SubMenuDesbloquearUsuario.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.SubMenuDesbloquearUsuario.Name).Descripcion;
            this.subMenuBitacora.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.subMenuBitacora.Name).Descripcion;
            this.subMenuDigitos.Text = traducciones.FirstOrDefault(t => t.Etiqueta == this.subMenuDigitos.Name).Descripcion;
        }

        private void MenuPrincipal_FormClosing(object sender, FormClosingEventArgs e)
        {
            Servicios.IdiomaManager.DesInscribir(this);
        }

        private void SubMenuBackup_Click(object sender, EventArgs e)
        {
            frmBackup frmBackup = new frmBackup();
            frmBackup.Show();
        }

        private void SubMenuRestore_Click(object sender, EventArgs e)
        {
            frmRestore frmRestore = new frmRestore();
            frmRestore.Show();
        }

        private void subMenuSalir_Click(object sender, EventArgs e)
        {
            frmLogOut frmLogOut = new frmLogOut();
            frmLogOut.Show();
        }

        private void SubMenuIdioma_Click(object sender, EventArgs e)
        {
            frmCambiarIdioma frmCambiarIdioma = new frmCambiarIdioma();
            frmCambiarIdioma.Show();
        }
    }
}
