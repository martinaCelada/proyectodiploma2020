﻿namespace Presentación
{
    partial class frmRestore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRestore = new System.Windows.Forms.Button();
            this.txtbd = new System.Windows.Forms.TextBox();
            this.lblNombrebd = new System.Windows.Forms.Label();
            this.lblArchivo = new System.Windows.Forms.Label();
            this.txtArchivo = new System.Windows.Forms.TextBox();
            this.btnArchivo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnRestore
            // 
            this.btnRestore.Location = new System.Drawing.Point(204, 119);
            this.btnRestore.Name = "btnRestore";
            this.btnRestore.Size = new System.Drawing.Size(129, 23);
            this.btnRestore.TabIndex = 5;
            this.btnRestore.Text = "Generar Restore";
            this.btnRestore.UseVisualStyleBackColor = true;
            this.btnRestore.Click += new System.EventHandler(this.btnRestore_Click);
            // 
            // txtbd
            // 
            this.txtbd.Location = new System.Drawing.Point(12, 122);
            this.txtbd.Name = "txtbd";
            this.txtbd.Size = new System.Drawing.Size(161, 20);
            this.txtbd.TabIndex = 4;
            // 
            // lblNombrebd
            // 
            this.lblNombrebd.AutoSize = true;
            this.lblNombrebd.Location = new System.Drawing.Point(9, 94);
            this.lblNombrebd.Name = "lblNombrebd";
            this.lblNombrebd.Size = new System.Drawing.Size(132, 13);
            this.lblNombrebd.TabIndex = 3;
            this.lblNombrebd.Text = "Nombre de base de datos:";
            // 
            // lblArchivo
            // 
            this.lblArchivo.AutoSize = true;
            this.lblArchivo.Location = new System.Drawing.Point(9, 24);
            this.lblArchivo.Name = "lblArchivo";
            this.lblArchivo.Size = new System.Drawing.Size(46, 13);
            this.lblArchivo.TabIndex = 6;
            this.lblArchivo.Text = "Archivo:";
            // 
            // txtArchivo
            // 
            this.txtArchivo.Location = new System.Drawing.Point(12, 52);
            this.txtArchivo.Name = "txtArchivo";
            this.txtArchivo.Size = new System.Drawing.Size(229, 20);
            this.txtArchivo.TabIndex = 7;
            // 
            // btnArchivo
            // 
            this.btnArchivo.Location = new System.Drawing.Point(258, 52);
            this.btnArchivo.Name = "btnArchivo";
            this.btnArchivo.Size = new System.Drawing.Size(75, 23);
            this.btnArchivo.TabIndex = 8;
            this.btnArchivo.Text = "Abrir Archivo";
            this.btnArchivo.UseVisualStyleBackColor = true;
            this.btnArchivo.Click += new System.EventHandler(this.btnArchivo_Click);
            // 
            // frmRestore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 179);
            this.Controls.Add(this.btnArchivo);
            this.Controls.Add(this.txtArchivo);
            this.Controls.Add(this.lblArchivo);
            this.Controls.Add(this.btnRestore);
            this.Controls.Add(this.txtbd);
            this.Controls.Add(this.lblNombrebd);
            this.Name = "frmRestore";
            this.Text = "frmRestore";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmRestore_FormClosing);
            this.Load += new System.EventHandler(this.frmRestore_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.TextBox txtbd;
        private System.Windows.Forms.Label lblNombrebd;
        private System.Windows.Forms.Label lblArchivo;
        private System.Windows.Forms.TextBox txtArchivo;
        private System.Windows.Forms.Button btnArchivo;
    }
}