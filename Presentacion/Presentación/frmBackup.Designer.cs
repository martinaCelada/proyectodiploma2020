﻿namespace Presentación
{
    partial class frmBackup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNombrebd = new System.Windows.Forms.Label();
            this.txtbd = new System.Windows.Forms.TextBox();
            this.btnBackup = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNombrebd
            // 
            this.lblNombrebd.AutoSize = true;
            this.lblNombrebd.Location = new System.Drawing.Point(9, 51);
            this.lblNombrebd.Name = "lblNombrebd";
            this.lblNombrebd.Size = new System.Drawing.Size(132, 13);
            this.lblNombrebd.TabIndex = 0;
            this.lblNombrebd.Text = "Nombre de base de datos:";
            // 
            // txtbd
            // 
            this.txtbd.Location = new System.Drawing.Point(12, 67);
            this.txtbd.Name = "txtbd";
            this.txtbd.Size = new System.Drawing.Size(151, 20);
            this.txtbd.TabIndex = 1;
            // 
            // btnBackup
            // 
            this.btnBackup.Location = new System.Drawing.Point(219, 64);
            this.btnBackup.Name = "btnBackup";
            this.btnBackup.Size = new System.Drawing.Size(129, 23);
            this.btnBackup.TabIndex = 2;
            this.btnBackup.Text = "Generar Backup";
            this.btnBackup.UseVisualStyleBackColor = true;
            this.btnBackup.Click += new System.EventHandler(this.btnBackup_Click);
            // 
            // frmBackup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(360, 149);
            this.Controls.Add(this.btnBackup);
            this.Controls.Add(this.txtbd);
            this.Controls.Add(this.lblNombrebd);
            this.Name = "frmBackup";
            this.Text = "frmBackup";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmBackup_FormClosing);
            this.Load += new System.EventHandler(this.frmBackup_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNombrebd;
        private System.Windows.Forms.TextBox txtbd;
        private System.Windows.Forms.Button btnBackup;
    }
}