﻿namespace Presentación
{
    partial class MenuPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.menuInicio = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuSalir = new System.Windows.Forms.ToolStripMenuItem();
            this.menuIngresos = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuProductos = new System.Windows.Forms.ToolStripMenuItem();
            this.menuVentas = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuRegistroVentas = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuClientes = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConsultas = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuDetalleVenta = new System.Windows.Forms.ToolStripMenuItem();
            this.menuReporte = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuReporteProducto = new System.Windows.Forms.ToolStripMenuItem();
            this.menuConfiguracion = new System.Windows.Forms.ToolStripMenuItem();
            this.SubMenuAdministrarUsuario = new System.Windows.Forms.ToolStripMenuItem();
            this.SubMenuAdministrarFamilia = new System.Windows.Forms.ToolStripMenuItem();
            this.SubMenuAdministrarPatente = new System.Windows.Forms.ToolStripMenuItem();
            this.SubMenuIdioma = new System.Windows.Forms.ToolStripMenuItem();
            this.SubMenuBackup = new System.Windows.Forms.ToolStripMenuItem();
            this.SubMenuRestore = new System.Windows.Forms.ToolStripMenuItem();
            this.SubMenuCambioContraseña = new System.Windows.Forms.ToolStripMenuItem();
            this.SubMenuDesbloquearUsuario = new System.Windows.Forms.ToolStripMenuItem();
            this.menuHerramientas = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuBitacora = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuDigitos = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuInicio,
            this.menuIngresos,
            this.menuVentas,
            this.menuConsultas,
            this.menuReporte,
            this.menuConfiguracion,
            this.menuHerramientas});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1236, 24);
            this.menuStrip.TabIndex = 1;
            this.menuStrip.Text = "MenuStrip";
            // 
            // menuInicio
            // 
            this.menuInicio.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuSalir});
            this.menuInicio.Name = "menuInicio";
            this.menuInicio.Size = new System.Drawing.Size(48, 20);
            this.menuInicio.Text = "Inicio";
            // 
            // subMenuSalir
            // 
            this.subMenuSalir.Name = "subMenuSalir";
            this.subMenuSalir.Size = new System.Drawing.Size(180, 22);
            this.subMenuSalir.Text = "Salir";
            this.subMenuSalir.Click += new System.EventHandler(this.subMenuSalir_Click);
            // 
            // menuIngresos
            // 
            this.menuIngresos.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuProductos});
            this.menuIngresos.Name = "menuIngresos";
            this.menuIngresos.Size = new System.Drawing.Size(63, 20);
            this.menuIngresos.Text = "Ingresos";
            // 
            // subMenuProductos
            // 
            this.subMenuProductos.Name = "subMenuProductos";
            this.subMenuProductos.Size = new System.Drawing.Size(128, 22);
            this.subMenuProductos.Text = "Productos";
            this.subMenuProductos.Click += new System.EventHandler(this.productosToolStripMenuItem_Click);
            // 
            // menuVentas
            // 
            this.menuVentas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuRegistroVentas,
            this.subMenuClientes});
            this.menuVentas.Name = "menuVentas";
            this.menuVentas.Size = new System.Drawing.Size(53, 20);
            this.menuVentas.Text = "Ventas";
            // 
            // subMenuRegistroVentas
            // 
            this.subMenuRegistroVentas.Name = "subMenuRegistroVentas";
            this.subMenuRegistroVentas.Size = new System.Drawing.Size(154, 22);
            this.subMenuRegistroVentas.Text = "Registro Ventas";
            // 
            // subMenuClientes
            // 
            this.subMenuClientes.Name = "subMenuClientes";
            this.subMenuClientes.Size = new System.Drawing.Size(154, 22);
            this.subMenuClientes.Text = "Clientes";
            this.subMenuClientes.Click += new System.EventHandler(this.clientesToolStripMenuItem_Click);
            // 
            // menuConsultas
            // 
            this.menuConsultas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuDetalleVenta});
            this.menuConsultas.Name = "menuConsultas";
            this.menuConsultas.Size = new System.Drawing.Size(71, 20);
            this.menuConsultas.Text = "Consultas";
            // 
            // subMenuDetalleVenta
            // 
            this.subMenuDetalleVenta.Name = "subMenuDetalleVenta";
            this.subMenuDetalleVenta.Size = new System.Drawing.Size(147, 22);
            this.subMenuDetalleVenta.Text = "Detalle ventas";
            // 
            // menuReporte
            // 
            this.menuReporte.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuReporteProducto});
            this.menuReporte.Name = "menuReporte";
            this.menuReporte.Size = new System.Drawing.Size(60, 20);
            this.menuReporte.Text = "Reporte";
            // 
            // subMenuReporteProducto
            // 
            this.subMenuReporteProducto.Name = "subMenuReporteProducto";
            this.subMenuReporteProducto.Size = new System.Drawing.Size(167, 22);
            this.subMenuReporteProducto.Text = "Reporte producto";
            // 
            // menuConfiguracion
            // 
            this.menuConfiguracion.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SubMenuAdministrarUsuario,
            this.SubMenuAdministrarFamilia,
            this.SubMenuAdministrarPatente,
            this.SubMenuIdioma,
            this.SubMenuBackup,
            this.SubMenuRestore,
            this.SubMenuCambioContraseña,
            this.SubMenuDesbloquearUsuario});
            this.menuConfiguracion.Name = "menuConfiguracion";
            this.menuConfiguracion.Size = new System.Drawing.Size(95, 20);
            this.menuConfiguracion.Text = "Configuracion";
            // 
            // SubMenuAdministrarUsuario
            // 
            this.SubMenuAdministrarUsuario.Name = "SubMenuAdministrarUsuario";
            this.SubMenuAdministrarUsuario.Size = new System.Drawing.Size(182, 22);
            this.SubMenuAdministrarUsuario.Text = "Administrar usuario";
            // 
            // SubMenuAdministrarFamilia
            // 
            this.SubMenuAdministrarFamilia.Name = "SubMenuAdministrarFamilia";
            this.SubMenuAdministrarFamilia.Size = new System.Drawing.Size(182, 22);
            this.SubMenuAdministrarFamilia.Text = "Administrar familia";
            // 
            // SubMenuAdministrarPatente
            // 
            this.SubMenuAdministrarPatente.Name = "SubMenuAdministrarPatente";
            this.SubMenuAdministrarPatente.Size = new System.Drawing.Size(182, 22);
            this.SubMenuAdministrarPatente.Text = "Administrar patente";
            // 
            // SubMenuIdioma
            // 
            this.SubMenuIdioma.Name = "SubMenuIdioma";
            this.SubMenuIdioma.Size = new System.Drawing.Size(182, 22);
            this.SubMenuIdioma.Text = "Idioma";
            this.SubMenuIdioma.Click += new System.EventHandler(this.SubMenuIdioma_Click);
            // 
            // SubMenuBackup
            // 
            this.SubMenuBackup.Name = "SubMenuBackup";
            this.SubMenuBackup.Size = new System.Drawing.Size(182, 22);
            this.SubMenuBackup.Text = "Backup";
            this.SubMenuBackup.Click += new System.EventHandler(this.SubMenuBackup_Click);
            // 
            // SubMenuRestore
            // 
            this.SubMenuRestore.Name = "SubMenuRestore";
            this.SubMenuRestore.Size = new System.Drawing.Size(182, 22);
            this.SubMenuRestore.Text = "Restore";
            this.SubMenuRestore.Click += new System.EventHandler(this.SubMenuRestore_Click);
            // 
            // SubMenuCambioContraseña
            // 
            this.SubMenuCambioContraseña.Name = "SubMenuCambioContraseña";
            this.SubMenuCambioContraseña.Size = new System.Drawing.Size(182, 22);
            this.SubMenuCambioContraseña.Text = "Cambio contraseña";
            // 
            // SubMenuDesbloquearUsuario
            // 
            this.SubMenuDesbloquearUsuario.Name = "SubMenuDesbloquearUsuario";
            this.SubMenuDesbloquearUsuario.Size = new System.Drawing.Size(182, 22);
            this.SubMenuDesbloquearUsuario.Text = "Desbloquear usuario";
            // 
            // menuHerramientas
            // 
            this.menuHerramientas.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuBitacora,
            this.subMenuDigitos});
            this.menuHerramientas.Name = "menuHerramientas";
            this.menuHerramientas.Size = new System.Drawing.Size(90, 20);
            this.menuHerramientas.Text = "&Herramientas";
            // 
            // subMenuBitacora
            // 
            this.subMenuBitacora.Name = "subMenuBitacora";
            this.subMenuBitacora.Size = new System.Drawing.Size(126, 22);
            this.subMenuBitacora.Text = "Bitacora";
            this.subMenuBitacora.Click += new System.EventHandler(this.bitacoraToolStripMenuItem_Click);
            // 
            // subMenuDigitos
            // 
            this.subMenuDigitos.Name = "subMenuDigitos";
            this.subMenuDigitos.Size = new System.Drawing.Size(126, 22);
            this.subMenuDigitos.Text = "DVV & DVH";
            // 
            // MenuPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1236, 477);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.Name = "MenuPrincipal";
            this.Text = "MenuPrincipal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuPrincipal_FormClosing);
            this.Load += new System.EventHandler(this.MenuPrincipal_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem menuInicio;
        private System.Windows.Forms.ToolStripMenuItem subMenuSalir;
        private System.Windows.Forms.ToolStripMenuItem menuIngresos;
        private System.Windows.Forms.ToolStripMenuItem subMenuProductos;
        private System.Windows.Forms.ToolStripMenuItem menuVentas;
        private System.Windows.Forms.ToolStripMenuItem subMenuRegistroVentas;
        private System.Windows.Forms.ToolStripMenuItem subMenuClientes;
        private System.Windows.Forms.ToolStripMenuItem menuConsultas;
        private System.Windows.Forms.ToolStripMenuItem subMenuDetalleVenta;
        private System.Windows.Forms.ToolStripMenuItem menuReporte;
        private System.Windows.Forms.ToolStripMenuItem subMenuReporteProducto;
        private System.Windows.Forms.ToolStripMenuItem menuConfiguracion;
        private System.Windows.Forms.ToolStripMenuItem SubMenuAdministrarUsuario;
        private System.Windows.Forms.ToolStripMenuItem SubMenuAdministrarFamilia;
        private System.Windows.Forms.ToolStripMenuItem SubMenuAdministrarPatente;
        private System.Windows.Forms.ToolStripMenuItem SubMenuIdioma;
        private System.Windows.Forms.ToolStripMenuItem SubMenuBackup;
        private System.Windows.Forms.ToolStripMenuItem SubMenuRestore;
        private System.Windows.Forms.ToolStripMenuItem SubMenuCambioContraseña;
        private System.Windows.Forms.ToolStripMenuItem SubMenuDesbloquearUsuario;
        private System.Windows.Forms.ToolStripMenuItem menuHerramientas;
        private System.Windows.Forms.ToolStripMenuItem subMenuBitacora;
        private System.Windows.Forms.ToolStripMenuItem subMenuDigitos;
    }
}