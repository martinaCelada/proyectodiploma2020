﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Negocio;
using Entidad;

namespace Presentación
{
    public partial class frmCliente : Form
    {
        ClienteBL clienteBL = new ClienteBL();
        public frmCliente()
        {
            InitializeComponent();
            CargarTabla();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Cliente c = new Cliente();
            c.nombre = txtNombre.Text;
            c.apellido = txtApellido.Text;
            c.DNI = Convert.ToInt32(txtDNI.Text);
            c.email = txtEmail.Text;

            if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtApellido.Text) && !string.IsNullOrEmpty(txtDNI.Text) && !string.IsNullOrEmpty(txtEmail.Text))
            {
                var validInsert = clienteBL.InsertarC(c);

                if (validInsert == true)
                {
                    MessageBox.Show("Cliente guardado correctamente");
                    CargarTabla();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Error!");
                }

            }
        }

        //completa los txt con los datos seleccionados de la grilla.
        private void tablaCliente_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (tablaCliente.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                tablaCliente.CurrentRow.Selected = true;
                txtCodigo.Text = tablaCliente.Rows[e.RowIndex].Cells["ID_cliente"].FormattedValue.ToString();
                txtNombre.Text = tablaCliente.Rows[e.RowIndex].Cells["nombre"].FormattedValue.ToString();
                txtApellido.Text = tablaCliente.Rows[e.RowIndex].Cells["apellido"].FormattedValue.ToString();
                txtDNI.Text = tablaCliente.Rows[e.RowIndex].Cells["DNI"].FormattedValue.ToString();
                txtEmail.Text = tablaCliente.Rows[e.RowIndex].Cells["email"].FormattedValue.ToString();
            }
        }

        private void Limpiar()
        {
            txtCodigo.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtDNI.Clear();
            txtEmail.Clear();
        }

        private void CargarTabla()
        {
            var cargarProductos = clienteBL.Listar_clientes();
            tablaCliente.DataSource = cargarProductos;
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            Cliente c = new Cliente();
            c.ID_cliente = Convert.ToInt32(txtCodigo.Text);
            c.nombre = txtNombre.Text;
            c.apellido = txtApellido.Text;
            c.DNI = Convert.ToInt32(txtDNI.Text);
            c.email = txtEmail.Text;

            if (!string.IsNullOrEmpty(txtCodigo.Text) && !string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtApellido.Text) && !string.IsNullOrEmpty(txtDNI.Text) && !string.IsNullOrEmpty(txtEmail.Text))
            {
                var validEdit = clienteBL.EditarC(c);

                if (validEdit == true)
                {
                    MessageBox.Show("Cliente editado correctamente");
                    CargarTabla();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Error!");
                }
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Cliente c = new Cliente();
            c.ID_cliente = Convert.ToInt32(txtCodigo.Text);

            if (!string.IsNullOrEmpty(txtCodigo.Text))
            {
                var validDelete = clienteBL.EliminarC(c);

                if (validDelete == true)
                {
                    MessageBox.Show("Cliente eliminado correctamente");
                    CargarTabla();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Error!");
                }
            }
        }
    }
}
