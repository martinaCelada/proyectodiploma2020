﻿using Entidad;
using Negocio;
using Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación
{
    public partial class frmCambiarIdioma : Form, IIdiomaObserver
    {
        private IdiomaManager IdiomaManager;
        private Entidad.Idioma idioma;
        public frmCambiarIdioma()
        {
            InitializeComponent();
        }

        public void ActualizarIdioma(Idioma idioma)
        {
            this.Traducir(idioma);
        }

        private void frmCambiarIdioma_Load(object sender, EventArgs e)
        {
            this.IdiomaManager = Servicios.IdiomaManager.ObtenerInstancia();
            Servicios.IdiomaManager.Suscribir(this);
        }

        private void frmCambiarIdioma_FormClosing(object sender, FormClosingEventArgs e)
        {
            Servicios.IdiomaManager.DesInscribir(this);
        }

        private void Traducir(Entidad.Idioma idioma = null)
        {
            Negocio.TraductorBL traductorBL = new TraductorBL();

            var traducciones = traductorBL.ObtenerTraducciones(idioma);

            foreach (Control control in this.Controls)
            {
                if (traducciones.Any(t => t.Etiqueta == control.Name))
                {
                    control.Text = traducciones.FirstOrDefault(t => t.Etiqueta == control.Name).Descripcion;
                }
            }
        }

        private void rdbIngles_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbIngles.Checked == true)
            {
                idioma = new Idioma
                {
                    Id = 2,
                    Nombre = "Ingles",
                    Principal = false

                };

                IdiomaManager.CambiarIdioma(idioma);
            }
        }

        private void rdbEspañol_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbEspañol.Checked == true)
            {
                idioma = new Idioma
                {
                    Id = 1,
                    Nombre = "Español",
                    Principal = true
                };

                IdiomaManager.CambiarIdioma(idioma);
            }
        }
    }
}
