﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad;
using Negocio;
using Servicios;

namespace Presentación
{
    public partial class frmProductos : Form, IIdiomaObserver
    {
        Entidad.Bitacora bitacora = new Entidad.Bitacora();
        BitacoraBL bitacoraBL = new BitacoraBL();
        ProductoBL ProductoBL = new ProductoBL();
        SesionManager sesion = SesionManager.ObtenerInstancia;
        
        public frmProductos()
        {
            InitializeComponent();
            CargarTabla();
        }

        private void frmProductos_Load(object sender, EventArgs e)
        {

        }

        private void CargarTabla()
        {
            var cargarProductos = ProductoBL.Listar_productos();
            tablaProducto.DataSource = cargarProductos;
        }

        private void tablaProducto_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (tablaProducto.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
            {
                tablaProducto.CurrentRow.Selected = true;
                txtID.Text = tablaProducto.Rows[e.RowIndex].Cells["ID_producto"].FormattedValue.ToString();
                txtNombre.Text = tablaProducto.Rows[e.RowIndex].Cells["nombre_producto"].FormattedValue.ToString();
                txtStock.Text = tablaProducto.Rows[e.RowIndex].Cells["stock"].FormattedValue.ToString();
                txtPrecioCompra.Text = tablaProducto.Rows[e.RowIndex].Cells["precio_compra"].FormattedValue.ToString();
                txtPrecioVenta.Text = tablaProducto.Rows[e.RowIndex].Cells["precio_venta"].FormattedValue.ToString();
                txtDescripcion.Text = tablaProducto.Rows[e.RowIndex].Cells["descripcion"].FormattedValue.ToString();

            }
        }

        private void Limpiar()
        {
            txtID.Clear();
            txtNombre.Clear();
            txtStock.Clear();
            txtPrecioCompra.Clear();
            txtPrecioVenta.Clear();
            txtDescripcion.Clear();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Producto p = new Producto();
            p.nombre_producto = txtNombre.Text;
            p.descripcion = txtDescripcion.Text;
            p.stock = Convert.ToInt32(txtStock.Text);
            p.precio_compra = Convert.ToInt32(txtPrecioCompra.Text);
            p.precio_venta = Convert.ToInt32(txtPrecioVenta.Text);

            if (!string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtDescripcion.Text) && !string.IsNullOrEmpty(txtStock.Text) && !string.IsNullOrEmpty(txtPrecioCompra.Text) && !string.IsNullOrEmpty(txtPrecioVenta.Text))
            {
                var validInsert = ProductoBL.InsertarP(p);

                if(validInsert == true)
                {
                    //agregamos el movimiento en la bitacora                    
                    bitacora.movimiento = "Alta producto";
                    bitacora.descripcion = $"El usuario {sesion.Usuario.nombre} dio de alta el producto {p.nombre_producto}";
                    bitacora.fecha_hora = DateTime.Now;
                    bitacora.ID_usuario = sesion.Usuario.ID_usuario;
                    bitacora.ID_criticidad = 3;

                   // bitacoraBL.AltaBitacora(bitacora);

                    MessageBox.Show("Producto guardado correctamente");
                    CargarTabla();
                    Limpiar();


                }
                else
                {
                    MessageBox.Show("Error!");
                }              
                
            }

            

        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            Producto p = new Producto();
            p.ID_producto = Convert.ToInt32(txtID.Text);
            p.nombre_producto = txtNombre.Text;
            p.descripcion = txtDescripcion.Text;
            p.stock = Convert.ToInt32(txtStock.Text);
            p.precio_compra = Convert.ToInt32(txtPrecioCompra.Text);
            p.precio_venta = Convert.ToInt32(txtPrecioVenta.Text);

            if (!string.IsNullOrEmpty(txtID.Text) &&  !string.IsNullOrEmpty(txtNombre.Text) && !string.IsNullOrEmpty(txtDescripcion.Text) && !string.IsNullOrEmpty(txtStock.Text) && !string.IsNullOrEmpty(txtPrecioCompra.Text) && !string.IsNullOrEmpty(txtPrecioVenta.Text))
            {
                var validEdit = ProductoBL.EditarP(p);

                if(validEdit == true)
                {
                    MessageBox.Show("Producto editado correctamente");
                    CargarTabla();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Error!");
                }
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Producto p = new Producto();
            p.ID_producto = Convert.ToInt32(txtID.Text);

            if (!string.IsNullOrEmpty(txtID.Text))
            {
                var validDelete = ProductoBL.EliminarP(p);

                if (validDelete == true)
                {
                    MessageBox.Show("Producto eliminado correctamente");
                    CargarTabla();
                    Limpiar();
                }
                else
                {
                    MessageBox.Show("Error!");
                }
            }
            
        }

        private void txtID_TextChanged(object sender, EventArgs e)
        {

        }

        private void tablaProducto_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmProductos_Load_1(object sender, EventArgs e)
        {
            Servicios.IdiomaManager.Suscribir(this);
            this.Traducir();
        }

        public void ActualizarIdioma(Entidad.Idioma idioma)
        {
            this.Traducir();
        }

        private void Traducir()
        {
            TraductorBL traductorBL = new TraductorBL();
            var idiomaManager = Servicios.IdiomaManager.ObtenerInstancia();

            var traducciones = traductorBL.ObtenerTraducciones(idiomaManager.idioma);

            foreach(Control item in this.Controls)
            {
                if(traducciones.Any(t => t.Etiqueta == item.Name))
                {
                    item.Text = traducciones.FirstOrDefault(t => t.Etiqueta == item.Name).Descripcion;
                }

                TraducirControlesInternos(item, traducciones);
            }
        }

        private void TraducirControlesInternos(Control item, List<Traduccion> traducciones)
        {
            if (item is GroupBox)
            {
                foreach (Control subItem in item.Controls)
                {
                    if (traducciones.Any(t => t.Etiqueta == subItem.Name))
                    {
                        subItem.Text = traducciones.FirstOrDefault(t => t.Etiqueta == subItem.Name).Descripcion;
                    }

                    TraducirControlesInternos(subItem, traducciones);
                }
            }
        }

        private void frmProductos_FormClosing(object sender, FormClosingEventArgs e)
        {
            Servicios.IdiomaManager.DesInscribir(this);
        }
    }
}
