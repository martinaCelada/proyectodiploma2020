﻿using Entidad;
using Negocio;
using Servicios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Presentación
{
    public partial class frmLogOut : Form, IIdiomaObserver
    {
        private IdiomaManager IdiomaManager;
        private Entidad.Idioma idioma;
        public frmLogOut()
        {
            InitializeComponent();
        }


        private void frmLogOut_Load(object sender, EventArgs e)
        {
            this.IdiomaManager = Servicios.IdiomaManager.ObtenerInstancia();
            Servicios.IdiomaManager.Suscribir(this);

        }

        public void ActualizarIdioma(Idioma idioma)
        {
            this.Traducir(idioma);
        }

        private void frmLogOut_FormClosing(object sender, FormClosingEventArgs e)
        {
            Servicios.IdiomaManager.DesInscribir(this);
        }

        private void Traducir(Entidad.Idioma idioma = null)
        {
            Negocio.TraductorBL traductorBL = new TraductorBL();

            var traducciones = traductorBL.ObtenerTraducciones(idioma);

            foreach (Control control in this.Controls)
            {
                if (traducciones.Any(t => t.Etiqueta == control.Name))
                {
                    control.Text = traducciones.FirstOrDefault(t => t.Etiqueta == control.Name).Descripcion;
                }
            }
        }

        private void TraducirControlesInternos(Control item, List<Traduccion> traducciones)
        {
            if (item is GroupBox)
            {
                foreach (Control subItem in item.Controls)
                {
                    if (traducciones.Any(t => t.Etiqueta == subItem.Name))
                    {
                        subItem.Text = traducciones.FirstOrDefault(t => t.Etiqueta == subItem.Name).Descripcion;
                    }

                    TraducirControlesInternos(subItem, traducciones);
                }
            }
        }

    }
}
