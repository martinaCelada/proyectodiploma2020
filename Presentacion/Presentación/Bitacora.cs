﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad;
using Negocio;
using Servicios;

namespace Presentación
{
    public partial class Bitacora : Form, IIdiomaObserver
    {
        BitacoraBL bitacoraBL = new BitacoraBL();
        private Servicios.SesionManager sesion;
        public Bitacora()
        {
            InitializeComponent();
           // CargarTabla();
        }

        private void Bitacora_Load(object sender, EventArgs e)
        {
            try
            {
                // sesion = Servicios.SesionManager.ObtenerInstancia();
                this.Traducir();
                Servicios.IdiomaManager.Suscribir(this);

                cmbCriticidad.DataSource = Enum.GetValues(typeof(Enums.Criticidad));

                Dictionary<string, string> comboUser = new Dictionary<string, string>();
                cmbUsuario.DisplayMember = "Value";
                cmbUsuario.ValueMember = "Key";
                comboUser.Add("0", "");

                //falta agregar usuario

                cmbUsuario.DataSource = new BindingSource(cmbUsuario, null);
                cmbUsuario.SelectedIndex = 0;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.BeginInvoke(new MethodInvoker(this.Close)); //no se que hace esto jeje
            }
        }

        //public void CargarTabla()
        //{
        //    var cargarBitacora = bitacoraBL.listar();
        //    tablaBitacora.DataSource = cargarBitacora;
        //}

       
        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Entidad.Enums.Criticidad criticidad = (Enums.Criticidad)cmbCriticidad.SelectedItem;

                string value = ((KeyValuePair<string, string>)cmbUsuario.SelectedItem).Key;

                ConsultarBitacoraRequest req = new ConsultarBitacoraRequest
                {
                    FechaDesde = this.dateTimeFechaDesde.Value.Date,
                    FechaHasta = this.dateTimeFechaHasta.Value.Date,
                    IdCriticidad = (int)criticidad,
                    IdUsuario = Convert.ToInt32(value)

                };

                List<Entidad.Bitacora> listaBitacora = bitacoraBL.Consultar(req);

                this.tablaBitacora.DataSource = listaBitacora.Select(x => new { Usuario = x.ID_usuario, Evento = x.descripcion, Fecha = x.fecha_hora, Criticidad = x.ID_criticidad }).ToList();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void TraducirControlesInternos(Control item, List<Traduccion> traducciones)
        {
            if (item is GroupBox)
            {
                foreach (Control subItem in item.Controls)
                {
                    if (traducciones.Any(t => t.Etiqueta == subItem.Name))
                    {
                        subItem.Text = traducciones.FirstOrDefault(t => t.Etiqueta == subItem.Name).Descripcion;
                    }

                    TraducirControlesInternos(subItem, traducciones);
                }
            }
        }

        private void Traducir()
        {
            TraductorBL traductorBL = new TraductorBL();

            var idiomaManager = Servicios.IdiomaManager.ObtenerInstancia();

            var traducciones = traductorBL.ObtenerTraducciones(idiomaManager.idioma);

            foreach (Control item in this.Controls)
            {
                if (traducciones.Any(t => t.Etiqueta == item.Name))
                {
                    item.Text = traducciones.FirstOrDefault(t => t.Etiqueta == item.Name).Descripcion;
                }

                TraducirControlesInternos(item, traducciones);
            }
        }

        public void ActualizarIdioma(Idioma idioma)
        {
            this.Traducir();
        }

        private void Bitacora_FormClosing(object sender, FormClosingEventArgs e)
        {
            Servicios.IdiomaManager.DesInscribir(this);
        }
    }
}
